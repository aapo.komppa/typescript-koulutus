export function demo(): string {
  const numberA: number = 2;
  const numberB: number = 4;

  return `multiplied: ${ multiply(numberA, numberB) }`;
}

function multiply(a: number, b:number): number{
  return a*b;
}

