import express = require('express');
import { demo } from './app';

// Create a new express app instance
const app: express.Application = express();

app.get('/', function (req, res) {
  res.send(demo());
});

app.listen(1337, function () {
  console.log('App is listening on port 1337!');
});